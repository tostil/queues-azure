﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.IO;

namespace Tostil.Tools.Queues.Azure
{
    public class AzureQueueAdapter : QueueAdapterBase
    {
        private Microsoft.WindowsAzure.Storage.Queue.CloudQueueClient _cloudClient;
        private Microsoft.WindowsAzure.Storage.Queue.CloudQueue _cloudQueue;

        #region Constructors
        public AzureQueueAdapter(IQueue queue)
            : base(queue)
        {
            InitializeElements();
        }

        public AzureQueueAdapter(string dataConnectionString, IQueue queue)
            : base(dataConnectionString, queue)
        {
            InitializeElements();
        }

        public override void Dispose()
        {
            base.Dispose();
            _cloudClient = null;
            _cloudQueue = null;
        }

        public void InitializeElements()
        {
            _cloudClient = GetActiveClient();
            _cloudQueue = GetQueue();
        }
        #endregion

        #region Private Methods
        private Microsoft.WindowsAzure.Storage.Queue.CloudQueueClient GetActiveClient()
        {
            if (_cloudClient == null)
            {
                _cloudClient = AzureQueueStorageUtils.GetClient(_dataConnectionString);
            }

            return _cloudClient;
        }

        private Microsoft.WindowsAzure.Storage.Queue.CloudQueue GetQueue()
        {
            if (_cloudQueue == null)
            {
                _cloudQueue = AzureQueueStorageUtils.GetQueueReference(GetActiveClient(), _queue.Name);
            }

            return _cloudQueue;                
        }
        #endregion

        protected override bool PushInternal(IQueueMessage item)
        {
            var success = true;
            try
            {
                AzureQueueStorageUtils.AddMessage(_cloudQueue, item.Content);
            }
            catch(Exception ex)
            {
                success = false;
            }

            return success;
        }

        protected override IQueueMessage PopInternal()
        {
            var azureMessage = AzureQueueStorageUtils.GetMessage(_cloudQueue);

            if (azureMessage != null && !string.IsNullOrEmpty(azureMessage.AsString))
            {
                AzureQueueStorageUtils.DeleteMessage(_cloudQueue, azureMessage);
            }
            
            return new AzureQueueMessage(azureMessage);
        }

        protected override IQueueMessage GetInternal()
        {
            var azureMessage = AzureQueueStorageUtils.GetMessage(_cloudQueue);
            return new AzureQueueMessage(azureMessage);
        }

        protected override bool DeleteInternal(IQueueMessage item)
        {
            var success = true;
            try
            {
                var azureMessage = ((AzureQueueMessage)item).AzureCloudMessage;
                _cloudQueue.DeleteMessage(azureMessage);
            }
            catch (Exception ex)
            {
                success = false;
            }

            return success;
        }        
    }
}
