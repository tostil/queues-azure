﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure.Storage.Queue;

namespace Tostil.Tools.Queues.Azure
{
    public class AzureQueueMessage : QueueMessageBase 
    {
        private CloudQueueMessage _azureCloudMessage;

        public CloudQueueMessage AzureCloudMessage
        {
            get { return _azureCloudMessage; }
            set { _azureCloudMessage = value; }
        }

         #region constructors
        public AzureQueueMessage() : base()
        { }

        internal AzureQueueMessage(CloudQueueMessage azureMessage)            
        {
            var toSet = string.Empty;
            if (azureMessage != null && !string.IsNullOrEmpty(azureMessage.AsString))
            {
                toSet = azureMessage.AsString;
            }
            this._content = toSet;
        }

        public AzureQueueMessage(string message)
            : base(message)
        {
        }

        #endregion
    }
}
