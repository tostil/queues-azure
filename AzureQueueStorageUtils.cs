﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage;

namespace Tostil.Tools.Queues.Azure
{
    internal static class AzureQueueStorageUtils
    {
        public static CloudQueueClient GetClient(string accountCnnStr)
        {
            var storageAccount = CloudStorageAccount.Parse(accountCnnStr);
            var queueClient = storageAccount.CreateCloudQueueClient();

            return queueClient;
        }

        public static CloudQueue GetQueueReference(CloudQueueClient queueClient, string queueName)
        {
            var queue = queueClient.GetQueueReference(queueName);
            if (!queue.Exists())
            {
                queue.CreateIfNotExists();
            }

            return queue;
        }

        public static CloudQueue GetQueueReference(string accountCnnStr, string queueName)
        {
            var storageAccount = CloudStorageAccount.Parse(accountCnnStr);
            var queueClient = storageAccount.CreateCloudQueueClient();
            var queue = queueClient.GetQueueReference(queueName);
            queue.CreateIfNotExists();

            return queue;
        }

        

        public static void AddMessage(CloudQueue queue, string content)
        {
            queue.AddMessage(new CloudQueueMessage(content));
        }

        public static CloudQueueMessage GetMessage(CloudQueue queue)
        {
            return GetMessage(queue, TimeSpan.FromSeconds(30));
        }

        public static CloudQueueMessage GetMessage(CloudQueue queue, TimeSpan visibilityTimeout)
        {
            return queue.GetMessage(visibilityTimeout);
        }

        public static void DeleteMessage(CloudQueue queue, CloudQueueMessage message)
        {
            queue.DeleteMessage(message);
        }

        public static void ClearQueue(CloudQueue queue)
        {
            queue.Clear();
        }

        public static int GetMessageCount(CloudQueue queue)
        {
            queue.FetchAttributes();
            return queue.ApproximateMessageCount ?? 0;
        }
        
    }
}
